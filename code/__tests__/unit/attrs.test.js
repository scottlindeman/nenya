const joi = require("joi");
const attrs = require("../../attrs.js");

// --- DATA --------------------------------------

const VnodeA = {
  attrs: {
    one: "hello",
    two: 2,
  },
};

const ValidatorA = joi.object({
  one: joi.string().required(),
});

// --- TESTS -------------------------------------

describe("joiExtract", function () {
  test("can extract a subset", function () {
    const result = attrs.joiExtract(VnodeA, ValidatorA);
    expect(Object.keys(result)).toBe(["one"]);
  });
});
