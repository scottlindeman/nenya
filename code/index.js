"use strict";

var attrs = require("./attrs.js");
var context = require("./context.js");

module.exports = {
  attrs,
  context,
};
