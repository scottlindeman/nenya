"use strict";

var joi = require("joi");

/**
 * A very scoped mithril vnode. We only care about attrs.
 * @typedef {object} MithrilVnode
 * @property {object} attrs - The attrs property
 */

/**
 * Extract the defined joi object from the attrs of a mithril vnode
 * @param {MithrilVnode} vnode - The vnode to extract attrs from
 * @param {object} joiDef - a joi validator
 * @returns {object} - The extracted values.
 */
function joiExtract(vnode, joiDef) {
  return joi.attempt(vnode.attrs, joiDef);
}

module.exports = {
  joiExtract,
};
