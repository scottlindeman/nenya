module.exports = {
  env: {
    browser: true,
    commonjs: true,
  },
  plugins: ["jsdoc"],
  extends: ["eslint:recommended", "plugin:jsdoc/recommended-error"],
  parserOptions: {
    ecmaVersion: "latest",
    sourceType: "module",
  },
};
